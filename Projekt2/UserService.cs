﻿using Projekt2.Models.DataModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Projekt2
{
    public class UserService
    {
        public async Task<ObservableCollection<User>> GetUsers()
        {
            var db = Singleton.Database;
            return await db.SelectList<User>();
        }

        public async Task<User?> GetUser(int id)
        {
            var db = Singleton.Database;
            return await db.Select<User>(id);
        }

        public async Task<bool> InsertUser(User user)
        {
            var db = Singleton.Database;
            Contact contact = new Contact();
            bool result = await db.Insert(contact);
            if (result)
            {
                user.ContactId = contact.Id;
                result = await db.Insert(user);
                if (result)
                {
                    contact.UserId = user.Id;
                    return await db.Update(contact);
                }
            }
            return false;
        }

        public async Task<bool> UpdateUser(User user)
        {
            var db = Singleton.Database;
            return await db.Update(user);
        }

        public async Task<bool> DeleteUser(User user)
        {
            var db = Singleton.Database;
            var invoiceService = new InvoiceService();
            var costService = new CostService();
            var contactService = new ContactService();
            var invoices = await invoiceService.GetInvoices((int)user.Id);
            var costs = await costService.GetCosts((int)user.Id);
            var contacts = await contactService.GetContacts((int)user.Id);
            foreach (var invoice in invoices)
            {
                bool result = await invoiceService.DeleteInvoice(invoice);
                if (!result)
                {
                    return false;
                }
            }
            foreach (var cost in costs)
            {
                bool result = await costService.DeleteCost(cost);
                if (!result)
                {
                    return false;
                }
            }
            foreach (var contact in contacts)
            {
                bool result = await contactService.DeleteContact(contact);
                if (!result)
                {
                    return false;
                }
            }
            return await db.Delete(user);
        }

        public async Task<bool> SetPassword(User user, string password, string passwordAgain)
        {
            if (password == passwordAgain)
            {
                user.PasswordHash = this.HashPassword(password);
                return true;
            }
            return false;
        }

        string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password, 10);
        }
    }
}
