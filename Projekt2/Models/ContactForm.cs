﻿using System.ComponentModel.DataAnnotations;

namespace Projekt2.Models
{
    public class ContactForm
    {
        public long? Id
        {
            get;
            set;
        }

        [Display(Name = "Typ kontaktu")]
        [Required(ErrorMessage = "Typ kontaktu je povinný údaj")]
        public int? Type
        {
            get;
            set;
        } = 0;

        [Display(Name = "Fyzická osoba")]
        [Required(ErrorMessage = "Typ osoby je povinný údaj")]
        public bool IsPersonal
        {
            get;
            set;
        } = true;

        [Display(Name = "Ulice")]
        [Required(ErrorMessage = "Ulice je povinný údaj")]
        public string? Street
        {
            get;
            set;
        }

        [Display(Name = "Č.P./Č.O")]
        [Required(ErrorMessage = "Č.P./Č.O je povinný údaj")]
        public string? StreetNo
        {
            get;
            set;
        }

        [Display(Name = "Obec")]
        [Required(ErrorMessage = "Obec je povinný údaj")]
        public string? City
        {
            get;
            set;
        }

        [Display(Name = "PSČ")]
        [Required(ErrorMessage = "PSČ je povinný údaj")]
        public int? PostalCode
        {
            get;
            set;
        }

        [Display(Name = "Země")]
        [Required(ErrorMessage = "Země je povinný údaj")]
        public string? CountryCode
        {
            get;
            set;
        }


        [Display(Name = "Společnost")]
        public string? Company
        {
            get;
            set;
        }

        [Display(Name = "Jméno")]
        public string? FirstName
        {
            get;
            set;
        }

        [Display(Name = "Příjmení")]
        public string? LastName
        {
            get;
            set;
        }

        [Display(Name = "DIČ")]
        public string? Dic
        {
            get;
            set;
        }


        [Display(Name = "IČ")]
        public int? Ic
        {
            get;
            set;
        }

        [Display(Name = "E-mail")]
        public string? Email
        {
            get;
            set;
        }

        [Display(Name = "Telefon")]
        public string? Phone
        {
            get;
            set;
        }
    }
}
