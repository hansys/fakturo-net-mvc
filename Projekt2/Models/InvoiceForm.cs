﻿using Projekt2.Models.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Projekt2.Models
{
    public class InvoiceForm
    {
        [Display(Name = "Odběratel")]
        [Required(ErrorMessage = "Odběratel je povinný údaj")]
        public long? SubscriberId
        {
            get;
            set;
        }

        [Display(Name = "Číslo faktury (Bude vygenerováno)")]
        public string? InvoiceNum
        {
            get;
            set;
        }

        [Display(Name = "Celkem bez DPH")]
        [Required(ErrorMessage = "Cena celkem bez DPH je povinný údaj")]
        [Range(0, float.MaxValue, ErrorMessage = "Cena celkem bez DPH musí být větší nebo rovno 0")]
        public double TotalPriceWithoutTax
        {
            get;
            set;
        } = 0;

        [Display(Name = "Celkem")]
        [Required(ErrorMessage = "Cena celkem je povinný údaj")]
        [Range(0, float.MaxValue, ErrorMessage = "Cena celkem musí být větší nebo rovno 0")]
        public double TotalPrice
        {
            get;
            set;
        } = 0;

        [Display(Name = "Celkem zaplaceno")]
        [Required(ErrorMessage = "Celkem zaplaceno je povinný údaj")]
        [Range(0, float.MaxValue, ErrorMessage = "Celkem zaplaceno musí být větší nebo rovno 0")]
        public double TotalPaidPrice
        {
            get;
            set;
        } = 0;

        [Display(Name = "Zaplacena")]
        public bool IsPaid
        {
            get;
            set;
        } = false;

        [Display(Name = "Stornovaná")]
        public bool IsCanceled
        {
            get;
            set;
        } = false;

        [Display(Name = "Poznámka")]
        public string? Note
        {
            get;
            set;
        }

        [Display(Name = "Datum vystavení")]
        [Required(ErrorMessage = "Datum vystavení je povinný údaj")]
        public DateTime CreatedTime
        {
            get;
            set;
        } = DateTime.Now;

        [Display(Name = "Datum splatnosti")]
        [Required(ErrorMessage = "Datum splatnosti je povinný údaj")]
        public DateTime PayTillTime
        {
            get;
            set;
        } = DateTime.Now.AddDays(14);

        [Display(Name = "Typ platby")]
        [Required(ErrorMessage = "Typ platby je povinný údaj")]
        public int PaymentType
        {
            get;
            set;
        } = 0;

        public List<InvoiceItemForm> Items
        {
            get;
            set;
        } = new List<InvoiceItemForm>();
    }
}
