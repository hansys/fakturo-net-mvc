﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt2.Models.DataModel
{
    public class InvoiceItem : BasicObject
    {
        long? invoiceId;
        [Column("InvoiceId")]
        public long? InvoiceId
        {
            get => invoiceId;
            set { invoiceId = value; Notify(); }
        }

        string? name;
        [Column("Name")]
        public string? Name
        {
            get => name;
            set { name = value; Notify(); }
        }

        int quantity = 1;
        [Column("Quantity")]
        public int Quantity
        {
            get => quantity;
            set { quantity = value; Notify("TotalPrice"); Notify(); }
        }

        string unit = "ks";
        [Column("Unit")]
        public string Unit
        {
            get => unit;
            set { unit = value; Notify(); }
        }

        int taxPercentageRate = 0;
        [Column("TaxPercentageRate")]
        public int TaxPercentageRate
        {
            get => taxPercentageRate;
            set { taxPercentageRate = value; Notify("TotalPrice"); Notify(); }
        }

        double price = 0;
        [Column("Price")]
        public double Price
        {
            get => price;
            set { price = value; Notify("TotalPrice"); Notify(); }
        }

        public double TotalPrice
        {
            get => Quantity * (price + price*taxPercentageRate/100);
        }

        public double TotalPriceWithoutTax
        {
            get => Quantity * price;
        }

        public bool Deleted
        {
            get;
            set;
        }

        public string ToHtmlString()
        {
            return $"<td>{Name}</td><td>{Quantity}</td><td>{Unit}</td><td>{Price} Kč</td><td>{taxPercentageRate}</td><td>{TotalPrice} Kč</td>";
        }
    }
}
