﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt2.Models.DataModel
{
    public class User : BasicObject
    {
        long? contactId;
        [Column("ContactId")]
        public long? ContactId
        {
            get => this.contactId;
            set { this.contactId = value; Notify(); }
        }

        int permission = 0;
        [Column("Permission")]
        public int Permission
        {
            get => this.permission;
            set { this.permission = value; Notify("PermissionString"); Notify(); }
        }

        public string PermissionString
        {
            get => this.permission == 0 ? "Uživatel" : "Administrátor";
        }

        public bool IsAdministrator
        {
            get => this.permission == 1;
        }

        string? username;
        [Column("Username")]
        public string? Username
        {
            get => this.username;
            set { this.username = value; Notify(); }
        }

        string? passwordHash;
        [Column("PasswordHash")]
        public string? PasswordHash
        {
            get => this.passwordHash;
            set { this.passwordHash = value; Notify(); }
        }
    }
}
