﻿using Projekt2.Models.Database;

namespace Projekt2.Models.DataModel
{
    public class Singleton
    {
        static Db? database;
        public static Db Database
        {
            get
            {
                if (Singleton.database == null)
                {
                    Singleton.database = new Db("database.db");
                }
                return Singleton.database;
            }
        }
    }
}
