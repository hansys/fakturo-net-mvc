﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Projekt2.Models.DataModel
{

    public class Contact : BasicObject, ICloneable
    {
        long? userId = 0;
        [Column("UserId")]
        public long? UserId
        {
            get => this.userId;
            set { this.userId = value; Notify(); }
        }

        bool isPersonal = true;
        [Column("IsPersonal")]
        public bool IsPersonal 
        {
            get => this.isPersonal;
            set { this.isPersonal = value; Notify(); }
        }

        int type = 0;
        [Column("Type")]
        public int Type
        {
            get => this.type;
            set { this.type = value; Notify("IsSupplier"); Notify("IsSubscriber"); Notify(); }
        }

        public bool IsSupplier
        {
            get => this.type == 0 || this.type == 2;
        }

        public bool IsSubscriber
        {
            get => this.type == 0 || this.type == 1;
        }

        string? street;
        [Column("Street")]
        public string? Street
        {
            get => this.street;
            set { this.street = value; Notify(); }
        }
        string? streetNo;
        [Column("StreetNo")]
        public string? StreetNo
        {
            get => this.streetNo;
            set { this.streetNo = value; Notify(); }
        }
        int? postalCode;
        [Column("PostalCode")]
        public int? PostalCode
        {
            get => this.postalCode;
            set { this.postalCode = value; Notify(); }
        }

        string? city;
        [Column("City")]
        public string? City
        {
            get => this.city;
            set { this.city = value; Notify(); }
        }
        string? countryCode;
        [Column("CountryCode")]
        public string? CountryCode
        {
            get => this.countryCode;
            set { this.countryCode = value; Notify(); }
        }

        string? company;
        [Column("Company")]
        public string? Company
        {
            get => this.company;
            set { this.company = value; Notify("Name"); Notify(); }
        }

        public string? Name {
            get => this.IsPersonal ? $"{this.FirstName} {this.LastName}" : this.company;
        }

        string? firstName;
        [Column("FirstName")]
        public string? FirstName { 
            get => this.firstName;
            set { this.firstName = value; Notify("Name"); Notify(); }
        }

        string? lastName;
        [Column("LastName")]
        public string? LastName
        {
            get => this.lastName;
            set { this.lastName = value; Notify("Name"); Notify(); }
        }

        string? dic;
        [Column("Dic")]
        public string? Dic
        {
            get => this.dic;
            set { this.dic = value; Notify(); }
        }
        int? ic;
        [Column("Ic")]
        public int? Ic
        {
            get => this.ic;
            set { this.ic = value; Notify(); }
        }

        string? phone;
        [Column("Phone")]
        public string? Phone
        {
            get => this.phone;
            set { this.phone = value; Notify(); }
        }
        string? email;
        [Column("Email")]
        public string? Email
        {
            get => this.email;
            set { this.email = value; Notify(); }
        }

        public string Address
        {
            get => $"{this.Street} {this.StreetNo}, {this.PostalCode} {this.City}, {this.CountryCode}";
        }

        public override string ToString()
        {
            return $"{this.Name} | {this.Address}";
        }


        public int TypeToIndex()
        {
            if (IsSubscriber && IsSupplier)
            {
                return 0;
            }
            if (IsSubscriber)
            {
                return 1;
            }
            if (IsSupplier)
            {
                return 2;
            }
            return 3;
        }

        public int CountryToIndex()
        {
            return (this.CountryCode == "SK") ? 1 : 0;
        }

        public void SetCountryFromIndex(int index)
        {
            this.CountryCode = index == 1 ? "SK" : "CZ";
        }

        public bool IsUserContact
        {
            get => this.type == 3;
        }
    }
}
