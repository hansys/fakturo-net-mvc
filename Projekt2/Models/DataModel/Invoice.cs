﻿using Projekt2.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt2.Models.DataModel
{
    public class Invoice : BasicObject
    {
        long? userId = 0;
        [Column("UserId")]
        public long? UserId
        {
            get => this.userId;
            set { this.userId = value; Notify(); }
        }

        Contact? subscriber;
        public Contact? Subscriber
        {
            get => subscriber;
            set { subscriber = value; Notify(); }
        }

        string? invoiceNum;
        [Column("InvoiceNum")]
        public string? InvoiceNum
        {
            get => invoiceNum;
            set { invoiceNum = value; Notify(); }
        }

        long? subscriberId;
        [Column("SubscriberId")]
        public long? SubscriberId
        {
            get => subscriberId;
            set { subscriberId = value; Notify("Subscriber"); Notify(); }
        }


        double totalPriceWithoutTax = 0;
        [Column("TotalPriceWithoutTax")]
        public double TotalPriceWithoutTax
        {
            get => totalPriceWithoutTax;
            set { totalPriceWithoutTax = value; Notify(); }
        }

        double totalPrice = 0;
        [Column("TotalPrice")]
        public double TotalPrice
        {
            get => totalPrice;
            set { totalPrice = value; Notify(); }
        }

        double totalPaidPrice = 0;
        [Column("TotalPaidPrice")]
        public double TotalPaidPrice
        {
            get => totalPaidPrice;
            set { 
                totalPaidPrice = value >= 0 && value <= totalPrice ? value : totalPrice;
                isPaid = totalPaidPrice == totalPrice;
                Notify("IsPaid");
                Notify("IsPaidString");
                Notify(); 
            }
        }

        bool isPaid = false;
        [Column("IsPaid")]
        public bool IsPaid
        {
            get => isPaid;
            set { isPaid = value; Notify(); }
        }

        public string IsPaidString
        {
            get => IsPaid ? "Zaplacená" : "Nezaplacená";
        }

        bool isCanceled = false;
        [Column("IsCanceled")]
        public bool IsCanceled
        {
            get => isCanceled;
            set { isCanceled = value; Notify(); }
        }

        string? note;
        [Column("Note")]
        public string? Note
        {
            get => note;
            set { note = value; Notify(); }
        }

        DateTime createdTime = DateTime.Now;
        [Column("CreatedTime")]
        public DateTime CreatedTime
        {
            get => createdTime;
            set { createdTime = value; Notify(); }
        }

        DateTime payTillTime = DateTime.Now.AddDays(14);
        [Column("PayTillTime")]
        public DateTime PayTillTime
        {
            get => payTillTime;
            set { payTillTime = value; Notify(); }
        }

        int paymentType = 0;
        [Column("PaymentType")]
        public int PaymentType
        {
            get => paymentType;
            set { paymentType = value; Notify(); }
        }

        public async Task LoadSubscriber(Db db)
        {
            this.subscriber = await db.Select<Contact>(this.subscriberId);
        }
    }
}
