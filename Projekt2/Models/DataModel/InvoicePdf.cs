﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Projekt2.Models.DataModel
{
    public class InvoicePdf
    {
        Contact? userContact;
        Contact? subscriberContact;
        ObservableCollection<InvoiceItem> items;
        Invoice invoice;
        string filename;
        string path;

        object lockObj = new object();

        public InvoicePdf(string path, Contact? userContact, Contact? subscriberContact, Invoice invoice, ObservableCollection<InvoiceItem> items)
        {
            this.userContact = userContact;
            this.subscriberContact = subscriberContact;
            this.invoice = invoice;
            this.items = items;
            this.filename = $"{invoice.InvoiceNum?.ToString()}.pdf";
            this.path = $"{path}/{invoice.CreatedTime.Year}/{invoice.CreatedTime.Month}/";
        }

        public void ConvertToPDF()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<style>");
            sb.Append("body{box-sizing:border-box;padding:3em 2em 2em 2em;font-size:18px;font-family:'Segoe UI', Verdana, Arial}");
            sb.Append("table{width:100%;border-collapse:collapse;border: 1px solid #000000;text-align:left;}");
            sb.Append("tr{width:100%;border-collapse:collapse;border: 1px solid #000000;}");
            sb.Append("td,th{padding:0.5em}");
            sb.Append("</style>");
            sb.Append($"<h1 style='text-align:right;'>Faktura {invoice.InvoiceNum}</h1>");

            sb.Append("<table style='margin: 0 0 1em 0';border: 0px solid transparent;><tr>");
            sb.Append($"<td style='text-align:left;border: 0px solid transparent;padding:1em;box-sizing:border-box;'><h3>Dodavatel</h3>{userContact?.Name}<br>{userContact?.Street} {userContact?.StreetNo}<br>{userContact?.PostalCode} {userContact?.City} {userContact?.CountryCode}<br>");
            if (userContact?.Ic != null)
            {
                sb.Append($"IČ: {userContact?.Ic} ");
            }
            if (userContact?.Dic != null && userContact?.Dic.Length > 0)
            {
                sb.Append($"DIČ: {userContact?.Dic}");
            }
            sb.Append($"<br>");
            if (userContact?.Email != null)
            {
                sb.Append($"E-mail: {userContact?.Email}<br>");
            }
            if (userContact?.Phone != null)
            {
                sb.Append($"Tel.: {userContact?.Phone}");
            }
            sb.Append($"</td>");

            sb.Append($"<td style='text-align:right;border: 0px solid transparent;padding:1em;box-sizing:border-box;'><h3>Odběratel</h3>{userContact?.Name}<br>{userContact?.Street} {userContact?.StreetNo}<br>{userContact?.PostalCode} {userContact?.City} {userContact?.CountryCode}<br>");
            if (subscriberContact?.Ic != null)
            {
                sb.Append($"IČ: {subscriberContact?.Ic} ");
            }
            if (subscriberContact?.Dic != null && subscriberContact?.Dic.Length > 0)
            {
                sb.Append($"DIČ: {subscriberContact?.Dic}");
            }
            sb.Append($"<br>");
            if (subscriberContact?.Email != null)
            {
                sb.Append($"E-mail: {subscriberContact?.Email}<br>");
            }
            if (subscriberContact?.Phone != null)
            {
                sb.Append($"Tel.: {subscriberContact?.Phone}");
            }
            sb.Append($"</td>");
            sb.Append("</tr></table>");

            sb.Append("<table style='margin: 0 0 4em 0';border: 0px solid transparent;><tr>");
            sb.Append($"<td style='text-align:left;border: 0px solid transparent;padding:1em;box-sizing:border-box;'>Datum vystavení: ");
            sb.Append(invoice.CreatedTime.ToString("dd.MM.yyyy"));
            sb.Append($"</td>");
            sb.Append($"<td style='text-align:right;border: 0px solid transparent;padding:1em;box-sizing:border-box;'>Datum splatnosti: ");
            sb.Append(invoice.PayTillTime.ToString("dd.MM.yyyy"));
            sb.Append($"</td>");
            sb.Append("</tr></table>");

            sb.Append($"<h4 style='font-weight:400;font-size:0.9em;margin: 0 0 1em 0;text-align:left;'>Poznámka: {invoice.Note}</h4>");

            sb.Append($"<h4 style='margin: 0 0 1em 0;text-align:left;'>Položky:</h4>");
            sb.Append("<table>");
            sb.Append("<thead><tr><th>Název</th><th>Počet</th><th>MJ</th><th>Cena</th><th>% DPH</th><th>Celkem</th></tr></thead><tbody>");
            foreach (var item in items)
            {
                sb.Append("<tr>");
                sb.Append(item.ToHtmlString());
                sb.Append("</tr>");
            }
            sb.Append("</tbody></table>");
            sb.Append("<div style='text-align:right;padding: 3em 0 0 0'>");
            sb.Append($"<h3 style='margin: 0 0 0.25em 0;'>Uhrazené zálohy:&nbsp;&nbsp;&nbsp;{invoice.TotalPaidPrice} Kč</h3>");
            sb.Append($"<h3 style='margin: 0 0 0.25em 0;'>Celkem bez DPH:&nbsp;&nbsp;&nbsp;{invoice.TotalPriceWithoutTax} Kč</h3>");
            sb.Append($"<h1 style='margin: 2em 0 0 0;'>Celkem k úhradě:&nbsp;&nbsp;&nbsp;{invoice.TotalPrice - invoice.TotalPaidPrice} Kč</h1>");
            sb.Append("</div>");
            var renderer = new IronPdf.ChromePdfRenderer();
            renderer.RenderingOptions.MarginTop = 0;
            renderer.RenderingOptions.MarginBottom = 0;
            renderer.RenderingOptions.MarginLeft = 0;
            renderer.RenderingOptions.MarginRight = 0;

            if (!Directory.Exists(this.path))
            {
                Directory.CreateDirectory(path);
            }
            renderer.RenderHtmlAsPdf(sb.ToString()).SaveAs(this.path + this.filename);
        }

    }
}
