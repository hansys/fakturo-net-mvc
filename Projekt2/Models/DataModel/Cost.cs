﻿using Projekt2.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt2.Models.DataModel
{
    public class Cost : BasicObject
    {
        long? userId = 0;
        [Column("UserId")]
        public long? UserId
        {
            get => this.userId;
            set { this.userId = value; Notify(); }
        }

        string? costNum;
        [Column("CostNum")]
        public string? CostNum
        {
            get => costNum;
            set { costNum = value; Notify(); }
        }

        string? originalNum;
        [Column("OriginalNum")]
        public string? OriginalNum
        {
            get => originalNum;
            set { originalNum = value; Notify(); }
        }

        Contact? supplier;
        public Contact? Supplier
        {
            get => supplier;
            set { supplier = value; Notify(); }
        }

        long? supplierId;
        [Column("SupplierId")]
        public long? SupplierId
        {
            get => this.supplierId;
            set { this.supplierId = value; Notify("Supplier"); Notify(); }
        }

        double? price = 0;
        [Column("Price")]
        public double? Price
        {
            get => this.price;
            set { this.price = value; Notify(); }
        }

        double? priceWithoutTax = 0;
        [Column("PriceWithoutTax")]
        public double? PriceWithoutTax
        {
            get => this.priceWithoutTax;
            set { this.priceWithoutTax = value; Notify(); }
        }

        string? fileLocation;
        [Column("FileLocation")]
        public string? FileLocation
        {
            get => this.fileLocation;
            set { this.fileLocation = value; Notify(); }
        }

        DateTime createdTime = DateTime.Now;
        [Column("CreatedTime")]
        public DateTime CreatedTime
        {
            get => createdTime;
            set { createdTime = value; Notify(); }
        }

        DateTime payTillTime = DateTime.Now;
        [Column("PayTillTime")]
        public DateTime PayTillTime
        {
            get => payTillTime;
            set { payTillTime = value; Notify(); }
        }

        string? note;
        [Column("Note")]
        public string? Note
        {
            get => note;
            set { note = value; Notify(); }
        }

        int type = 0;
        [Column("Type")]
        public int Type
        {
            get => this.type;
            set { this.type = value; Notify(); }
        }

        public async Task LoadSupplier(Db db)
        {
            this.supplier = await db.Select<Contact>(this.supplierId);
        }
    }
}
