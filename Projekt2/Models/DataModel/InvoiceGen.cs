﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Projekt2.Models.DataModel
{
    public class InvoiceGen
    {
        AppState appState;
        ObservableCollection<Invoice> invoices;
        
        public InvoiceGen(AppState appState, ObservableCollection<Invoice> invoices)
        {
            this.appState = appState;
            this.invoices = invoices;
        }
        public async Task Generate()
        {
            int generated = 0;
            while (generated != this.invoices.Count)
            {
                List<Thread> threads = new List<Thread>();
                for(int i = generated; i < generated + 10; i++)
                {
                    if (i < this.invoices.Count)
                    {
                        Invoice invoice = this.invoices[i];
                        Contact? userContact = this.appState.LoggedUserContact;
                        Contact? subscriberContact = await this.appState.Database.Select<Contact>(invoice.SubscriberId ?? -1);
                        ObservableCollection<InvoiceItem> items = await this.appState.Database.SelectList<InvoiceItem>(invoice.Id ?? -1, "InvoiceId");
                        InvoicePdf invoicePdf = new InvoicePdf("./export", userContact, subscriberContact, invoice, items);

                        Thread threadTmp = new Thread(invoicePdf.ConvertToPDF);
                        threads.Add(threadTmp);
                        threadTmp.Start();

                    }
                }
                foreach (var thread in threads)
                {
                    thread.Join();
                    generated++;
                }
            }
        }
    }
}

