﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt2.Models.Database
{
    public class DbLogger
    {
        public List<string> Errors { get; protected set; }
             
        public DbLogger()
        {
            Errors = new List<string>();
        }

        public void Add(string message)
        {
            Errors.Add(message);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var errorMessage in Errors)
            {
                sb.Append($"ERROR: {errorMessage}\n");
            }
            return sb.ToString();
        }
    }
}
