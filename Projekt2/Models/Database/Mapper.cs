﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Globalization;
using System.Dynamic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;

namespace Projekt2.Models.Database
{
    // Database ORM mapper
    public class Mapper
    {
        #nullable enable

        [DllImport("Kernel32")]
        public static extern void AllocConsole();

        [DllImport("Kernel32")]
        public static extern void FreeConsole();

        protected static object? ConvertorFromDb(object? obj, Type type)
        {
            if (obj == DBNull.Value)
            {
                return null;
            }
            if (type == typeof(bool?) || type == typeof(bool))
            {
                return Convert.ToBoolean(obj);
            }
            if (type == typeof(string))
            {
                return Convert.ToString(obj);
            }
            if (type == typeof(double?) || type == typeof(double))
            {
                return Convert.ToDouble(obj);
            }
            if (type == typeof(int?) || type == typeof(int))
            {
                return Convert.ToInt32(obj);
            }
            if (type == typeof(long?) || type == typeof(long))
            {
                return Convert.ToInt64(obj);
            }
            if ((type == typeof(DateTime) || type == typeof(DateTime?)) && obj != null)
            {
                double unixTimestamp = Convert.ToDouble(obj);
                return UnixTimestampToDateTime(unixTimestamp);
            }
            return null;
        }

        protected static object? ConvertorToDb(object? value, Type type)
        {
            if (type == typeof(bool?) || type == typeof(bool))
            {
                return Convert.ToInt32(value);
            }
            if (type == typeof(DateTime?) || type == typeof(DateTime))
            {
                if (value != null)
                {
                    return DateTimeToUnixTimestamp((DateTime)value);
                }
            }
            return value;
        }
        protected static bool ObjectHasKey(object obj, string primaryKey)
        {
            Type objectType = obj.GetType();
            return  obj != null &&
                    objectType.GetProperty(primaryKey) != null &&
                    objectType.GetProperty(primaryKey)?.GetValue(obj) != null;
        }

        public static async Task<bool> Insert(DbConnection connection, object obj, string primaryKey = "Id")
        {
            Type objectType = obj.GetType();
            PropertyInfo? primaryKeyProperty = objectType.GetProperty(primaryKey);

            if (obj != null && primaryKeyProperty != null && objectType.GetProperties().Length > 0)
            {
                DbCommand cmd = connection.CreateCommand();
                var properties = objectType.GetProperties().Where(property => property.Name != primaryKey && property.CanWrite && property.GetCustomAttribute<ColumnAttribute>() != null);

                string? primaryKeyColumn = objectType.GetProperty(primaryKey)?.GetCustomAttribute<ColumnAttribute>()?.Name;

                StringBuilder sb = new StringBuilder();
                sb.Append($"INSERT INTO {objectType.Name} (");
                sb.Append(string.Join(",", properties.Select(property => $"{property.GetCustomAttribute<ColumnAttribute>()?.Name}")));
                sb.Append(") VALUES (");
                sb.Append(string.Join(",", properties.Select(property => $"@{property.Name}")));
                sb.Append(") ");
                sb.Append($"RETURNING {primaryKeyColumn}");
                cmd.CommandText = sb.ToString();

                foreach (PropertyInfo property in objectType.GetProperties())
                {
                    if (property.Name != primaryKey)
                    {
                        object? value = objectType.GetProperty(property.Name)?.GetValue(obj);
                        cmd.AddParameterWithValue(
                            $"@{property.Name}",
                            ConvertorToDb(value, property.PropertyType)
                        );
                    }                
                }

                try
                {
                    object? result = await cmd.ExecuteScalarAsync();
                    if (result != null)
                    {
                        long generatedId = Convert.ToInt64(result);
                        primaryKeyProperty.SetValue(obj,
                            ConvertorFromDb(
                                generatedId,
                                primaryKeyProperty.PropertyType
                            )
                        );
                        MethodInfo? notifyInserted = objectType.GetMethod("NotifyInserted");
                        if (notifyInserted != null)
                        {
                            notifyInserted.Invoke(obj, null);
                        }
                    }
                    return true;
                }
                catch (Exception e)
                {
                    AllocConsole();
                    Console.WriteLine($"{objectType.Name}:{e.Message}");
                }
                
            }
            return false;
        }

        public static async Task<bool> Update(DbConnection connection, object obj, string primaryKey = "Id")
        {
            Type objectType = obj.GetType();
            PropertyInfo? primaryKeyProperty = objectType.GetProperty(primaryKey);

            if (
                obj != null &&
                primaryKeyProperty != null &&
                Mapper.ObjectHasKey(obj, primaryKey)
            )
            {
                DbCommand cmd = connection.CreateCommand();
                var properties = objectType.GetProperties().Where(property => property.Name != primaryKey && property.CanWrite && property.GetCustomAttribute<ColumnAttribute>() != null);

                StringBuilder sb = new StringBuilder();
                sb.Append($"UPDATE {objectType.Name} SET ");
                sb.Append(string.Join(",", properties.Select(property => $"{property.GetCustomAttribute<ColumnAttribute>()?.Name} = @{property.Name}")));
                sb.Append($" WHERE {primaryKeyProperty.GetCustomAttribute<ColumnAttribute>()?.Name} = @{primaryKeyProperty.Name}");
                cmd.CommandText = sb.ToString();

                foreach (PropertyInfo property in objectType.GetProperties())
                {
                    if (property.Name != primaryKey)
                    {
                        object? value = objectType.GetProperty(property.Name)?.GetValue(obj);
                        cmd.AddParameterWithValue(
                            $"@{property.Name}",
                            ConvertorToDb(value, property.PropertyType)
                        );
                    }
                }
                cmd.AddParameterWithValue(
                    $"@{primaryKeyProperty.Name}", 
                    ConvertorToDb(
                        primaryKeyProperty.GetValue(obj),
                        primaryKeyProperty.PropertyType
                    )
                );
                try
                {
                    var result = await cmd.ExecuteNonQueryAsync() > 0;
                    MethodInfo? notifyUpdated = objectType.GetMethod("NotifyUpdated");
                    if (notifyUpdated != null && result)
                    {
                        notifyUpdated.Invoke(obj, null);
                    }
                    return result;
                }
                catch (Exception e)
                {
                    AllocConsole();
                    Console.WriteLine($"{objectType.Name}:{e.Message}");
                }
            }
            return false;
        }

        public static async Task<bool> Delete(DbConnection connection, object obj, string primaryKey = "Id")
        {
            Type objectType = obj.GetType();
            PropertyInfo? primaryKeyProperty = objectType.GetProperty(primaryKey);

            if (
                obj != null &&
                primaryKeyProperty != null &&
                Mapper.ObjectHasKey(obj, primaryKey)
            )
            {
                DbCommand cmd = connection.CreateCommand();
                StringBuilder sb = new StringBuilder();
                sb.Append($"DELETE FROM {objectType.Name} ");
                sb.Append($"WHERE {primaryKeyProperty.GetCustomAttribute<ColumnAttribute>()?.Name} = @{primaryKeyProperty.Name}");
                cmd.CommandText = sb.ToString();

                cmd.AddParameterWithValue(
                    $"@{primaryKeyProperty.Name}",
                    ConvertorToDb(
                        primaryKeyProperty.GetValue(obj),
                        primaryKeyProperty.PropertyType
                    )
                );
                try
                {
                    bool result = await cmd.ExecuteNonQueryAsync() > 0;
                    if (result)
                    {
                        primaryKeyProperty.SetValue(obj, null);
                        MethodInfo? notifyDeleted = objectType.GetMethod("NotifyDeleted");
                        if (notifyDeleted != null)
                        {
                            notifyDeleted.Invoke(obj, null);
                        }
                    }
                    return result;
                }
                catch (Exception e)
                {
                    AllocConsole();
                    Console.WriteLine($"{objectType.Name}:{e.Message}");
                }
            }
            return false;
        }

        public static async Task<ObservableCollection<T>> SelectList<T>(DbConnection connection, object? primaryKeyValue = null, string primaryKey = "Id", uint limit = 500, uint offset = 0, bool orderByDesc = true)
        {
            ObservableCollection<T> list = new ObservableCollection<T>();
            Type objectType = typeof(T);
            PropertyInfo? primaryKeyProperty = objectType.GetProperty(primaryKey);

            if (primaryKeyProperty != null)
            {
                DbCommand cmd = connection.CreateCommand();
                StringBuilder sb = new StringBuilder();
                sb.Append($"SELECT * FROM {objectType.Name} ");
                if (primaryKeyValue != null)
                {
                    sb.Append($"WHERE {primaryKeyProperty.GetCustomAttribute<ColumnAttribute>()?.Name} = @{primaryKeyProperty.Name} ");
                }
                string orderDirection = orderByDesc ? "DESC" : "ASC";
                sb.Append($"ORDER BY {primaryKeyProperty.GetCustomAttribute<ColumnAttribute>()?.Name} {orderDirection} LIMIT {limit} OFFSET {offset}");
                cmd.CommandText = sb.ToString();

                if (primaryKeyValue != null)
                {
                    cmd.AddParameterWithValue($"@{primaryKeyProperty.Name}", primaryKeyValue);
                }

                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {

                        try
                        {
                            object? objectInstance = Activator.CreateInstance(objectType);
                            if (objectInstance != null)
                            {
                                foreach (PropertyInfo property in objectType.GetProperties())
                                {
                                    ColumnAttribute? column = property.GetCustomAttribute<ColumnAttribute>();
                                    if (column != null && column.Name != null)
                                    {
                                        int ordinal = reader.GetOrdinal(column.Name);
                                        if (ordinal >= 0)
                                        {
                                            object value = reader.GetValue(ordinal);
                                            if (property.CanWrite)
                                            {
                                                property.SetValue(
                                                    objectInstance,
                                                    ConvertorFromDb(value, property.PropertyType),
                                                    null
                                                );
                                            }
                                        }
                                    }

                                }
                                list.Add((T)objectInstance);
                            }
                        }
                        catch (Exception e)
                        {
                            AllocConsole();
                            Console.WriteLine($"{objectType.Name}:{e.Message}");
                        }  
                    }
                }
            }
            return list;
        }

        public static async Task<int?> Count<T>(DbConnection connection, long userId, object? primaryKeyValue = null, string primaryKey = "Id")
        {
            Type objectType = typeof(T);
            PropertyInfo? primaryKeyProperty = objectType.GetProperty(primaryKey);

            if (primaryKeyProperty != null)
            {
                DbCommand cmd = connection.CreateCommand();
                StringBuilder sb = new StringBuilder();
                sb.Append($"SELECT COUNT(*) FROM {objectType.Name} ");
                if (primaryKeyValue != null)
                {
                    sb.Append($"WHERE {primaryKeyProperty.GetCustomAttribute<ColumnAttribute>()?.Name} >= @{primaryKeyProperty.Name} AND UserId = {userId}");
                }
                cmd.CommandText = sb.ToString();

                if (primaryKeyValue != null)
                {
                    cmd.AddParameterWithValue($"@{primaryKeyProperty.Name}", primaryKeyValue);
                }

                var result = await cmd.ExecuteScalarAsync();
                if (result != null)
                {
                    return Convert.ToInt32(result);
                }

            }
            return null;
        }


        public static async Task<T?> Select<T>(DbConnection connection, object? primaryKeyValue = null, string primaryKey = "Id")
        {
            var results = await Mapper.SelectList<T>(connection, primaryKeyValue, primaryKey, 1, 0);
            return results.Count > 0 ? results[0] : default(T);
        }

        public static DateTime UnixTimestampToDateTime(double unixTime)
        {
            DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Local);
            long unixTimeStampInTicks = (long)(unixTime * TimeSpan.TicksPerSecond);
            return new DateTime(unixStart.Ticks + unixTimeStampInTicks, System.DateTimeKind.Local);
        }

        public static double DateTimeToUnixTimestamp(DateTime dateTime)
        {
            DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Local);
            long unixTimeStampInTicks = (dateTime - unixStart).Ticks;
            return (double)unixTimeStampInTicks / TimeSpan.TicksPerSecond;
        }

        public static Target CopyProperties<Source, Target>(Source source, Target target)
        {
            foreach (var sProp in source.GetType().GetProperties())
            {
                bool isMatched = target.GetType().GetProperties().Any(tProp => tProp.Name == sProp.Name && tProp.GetType() == sProp.GetType() && tProp.CanWrite);
                if (isMatched)
                {
                    var value = sProp.GetValue(source);
                    PropertyInfo propertyInfo = target.GetType().GetProperty(sProp.Name);
                    propertyInfo?.SetValue(target, value);
                }
            }
            return target;
        }
    }
}
