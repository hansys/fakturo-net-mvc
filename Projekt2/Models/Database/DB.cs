﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace Projekt2.Models.Database
{
    // Database object
    public class Db
    {
        #nullable enable

        string fileName;
        public DbLogger Logger { get; protected set; }
        public Db(string fileName)
        {
            this.fileName = fileName;
            this.Logger = new DbLogger();
        }


        public DbConnection ConnectionCreator()
        {
            return new SqliteConnection($"Data Source={this.fileName}");
        }

        public async Task<bool> Insert(object obj)
        {
            using (var connection = this.ConnectionCreator())
            {
                connection.Open();
                return await Mapper.Insert(connection, obj);
            }
        }

        public async Task<bool> Update(object obj)
        {
            using (var connection = this.ConnectionCreator())
            {
                connection.Open();
                return await Mapper.Update(connection, obj);
            }
        }


        public async Task<bool> Delete(object obj)
        {
            using (var connection = this.ConnectionCreator())
            {
                connection.Open();
                return await Mapper.Delete(connection, obj);
            }
        }

        public async Task<T?> Select<T>(object keyValue, string key = "Id")
        {
            using (var connection = this.ConnectionCreator())
            {
                connection.Open();
                return await Mapper.Select<T>(connection, keyValue, key);
            }
        }

        public async Task<ObservableCollection<T>> SelectList<T>(object? keyValue = null, string key = "Id", uint limit = 500)
        {
            using (var connection = this.ConnectionCreator())
            {
                connection.Open();
                return await Mapper.SelectList<T>(connection, keyValue, key);
            }
        }

        public async Task<int?> Count<T>(long userId, object? keyValue = null, string key = "Id")
        {
            using (var connection = this.ConnectionCreator())
            {
                connection.Open();
                return await Mapper.Count<T>(connection, userId, keyValue, key);
            }
        }

    }
}
