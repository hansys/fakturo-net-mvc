﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Projekt2.Models
{
    public class CostForm
    {
        [Display(Name = "Číslo výdaje (Bude vygenerováno)")]
        public string? CostNum
        {
            get;
            set;
        }

        [Display(Name = "Původní číslo dokladu")]
        public string? OriginalNum
        {
            get;
            set;
        }

        [Display(Name = "Dodavatel")]
        [Required(ErrorMessage = "Dodavatel je povinný údaj")]
        public long? SupplierId
        {
            get;
            set;
        }

        [Display(Name = "Cena")]
        [Required(ErrorMessage = "Cena je povinný údaj")]
        [Range(0, float.MaxValue, ErrorMessage = "Cena musí být větší nebo rovno 0")]
        public double? Price
        {
            get;
            set;
        } = 0;

        [Display(Name = "Cena bez DPH")]
        [Required(ErrorMessage = "Cena bez DPH je povinný údaj")]
        [Range(0, float.MaxValue, ErrorMessage = "Cena bez DPH musí být větší nebo rovno 0")]
        public double? PriceWithoutTax
        {
            get;
            set;
        } = 0;

        [Display(Name = "Datum přijetí")]
        [Required(ErrorMessage = "Datum přijetí je povinný údaj")]
        public DateTime CreatedTime
        {
            get;
            set;
        } = DateTime.Now;

        [Display(Name = "Datum splatnost")]
        [Required(ErrorMessage = "Datum splatnost je povinný údaj")]
        public DateTime PayTillTime
        {
            get;
            set;
        } = DateTime.Now.AddDays(14);

        [Display(Name = "Poznámka")]
        public string? Note
        {
            get;
            set;
        }

        [Display(Name = "Typ dokladu")]
        [Required(ErrorMessage = "Typ dokladu je povinný údaj")]
        public int Type
        {
            get;
            set;
        } = 0;

    }
}
