﻿using System.ComponentModel.DataAnnotations;

namespace Projekt2.Models
{
    public class InvoiceItemForm
    {
        public long? Id
        {
            get;
            set;
        }

        [Display(Name = "Název")]
        [Required(ErrorMessage = "Název je povinný údaj")]
        public string? Name
        {
            get;
            set;
        }

        [Display(Name = "Počet")]
        [Required(ErrorMessage = "Počet je povinný údaj")]
        [Range(0, int.MaxValue, ErrorMessage = "Počet musí být větší nebo roven 0")]
        public int Quantity
        {
            get;
            set;
        } = 1;

        [Display(Name = "MJ")]
        [Required(ErrorMessage = "MJ je povinný údaj")]
        public string Unit
        {
            get;
            set;
        } = "ks";


        [Display(Name = "DPH")]
        [Required(ErrorMessage = "DPH je povinný údaj")]
        [Range(0, 100, ErrorMessage = "Přípustné hodnoty jsou 0 - 100")]
        public int TaxPercentageRate
        {
            get;
            set;
        } = 0;

        [Display(Name = "Cena")]
        [Required(ErrorMessage = "Cena je povinný údaj")]
        [Range(0, float.MaxValue, ErrorMessage = "Cena musí být větší nebo rovna 0")]
        public double Price
        {
            get;
            set;
        } = 0;

        public bool Deleted
        {
            get;
            set;
        }

        public double TotalPrice
        {
            get => Quantity * Price + (Price * TaxPercentageRate / 100);
        }

        public double TotalPriceWithoutTax
        {
            get => Quantity * Price;
        }

    }
}
