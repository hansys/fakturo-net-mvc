﻿using System.ComponentModel.DataAnnotations;

namespace Projekt2.Models
{
    public class LoginForm
    {
        [Display(Name = "Uživatelské jméno")]
        [Required(ErrorMessage = "Vyplňte uživatelské jméno")]
        public string? Username
        {
            get;
            set;
        }

        [Display(Name = "Heslo")]
        [Required(ErrorMessage = "Vyplňte heslo")]
        public string? Password
        {
            get;
            set;
        }
    }
}
