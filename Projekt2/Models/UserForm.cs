﻿using System.ComponentModel.DataAnnotations;

namespace Projekt2.Models
{
    public class UserForm
    {
        [Display(Name = "Oprávnění")]
        [Required(ErrorMessage = "Vyplňte oprávnění")]
        public int Permission
        {
            get;
            set;
        } = 0;

        [Display(Name = "Uživatelské jméno")]
        [Required(ErrorMessage = "Vyplňte uživatelské jméno")]
        [MinLength(4, ErrorMessage = "Minimální délka jména jsou 4 znaky")]
        public string? Username
        {
            get;
            set;
        }

        [Display(Name = "Heslo")]
        public string? Password
        {
            get;
            set;
        }

        [Display(Name = "Heslo znovu")]
        public string? PasswordAgain
        {
            get;
            set;
        }
    }
}
