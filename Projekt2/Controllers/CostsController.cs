﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Projekt2.Models;
using Projekt2.Models.Database;
using Projekt2.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projekt2.Controllers
{
    public class CostsController : Controller
    {
        [HttpGet]
        [ActionName("Index")]
        public async Task<IActionResult> IndexGet([FromServices] LoginService loginService, [FromServices] CostService costService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            var userId = this.HttpContext.Session.GetInt32("userId");
            var costs = await costService.GetCosts(userId ?? -1);
            ViewBag.Costs = costs;
            return View();
        }

        [HttpPost]
        [ActionName("Index")]
        public async Task<IActionResult> IndexPost([FromServices] LoginService loginService, [FromServices] CostService costService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            var userId = this.HttpContext.Session.GetInt32("userId");
            if (this.HttpContext.Request.Form.ContainsKey("deleteCost"))
            {
                int id = Convert.ToInt32(this.HttpContext.Request.Form["deleteCost"]);
                var cost = await costService.GetCost(id);
                if (cost != null && cost.UserId == userId)
                {
                    bool result = await costService.DeleteCost(cost);
                    if (result)
                    {
                        ViewData["Success"] = "Výdaj odstraněn";
                    }
                    else
                    {
                        ViewData["Error"] = "Výdaj se nepodařilo odstranit";
                    }
                }
            }
            var costs = await costService.GetCosts(userId ?? -1);
            ViewBag.Costs = costs;
            return View();
        }

        [HttpGet]
        [ActionName("Edit")]
        public async Task<IActionResult> EditGet(int? id, [FromServices] LoginService loginService, [FromServices] ContactService contactService, [FromServices] CostService costService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            var userId = this.HttpContext.Session.GetInt32("userId");
            CostForm costForm = new CostForm();
            Cost? cost = id == null ? new Cost() : await costService.GetCost(id ?? -1);
            if (id != null && (cost == null || cost.UserId != userId))
            {
                ViewData["Error"] = "Tento výdaj neexistuje";
                return await this.RenderForm((int)userId, cost != null ? (int?)cost.Id : null, costForm, contactService);
            }
            if (this.HttpContext.Session.GetInt32("alert_costAdded") != null)
            {
                this.HttpContext.Session.Remove("alert_costAdded");
                ViewData["Success"] = "Výdaj byl uložen";
            }
            Mapper.CopyProperties(cost, costForm);
            return await this.RenderForm((int)userId, cost != null ? (int?)cost.Id : null, costForm, contactService);
        }

        [HttpPost]
        [ActionName("Edit")]
        public async Task<IActionResult> EditPost(int? id, CostForm costForm, [FromServices] LoginService loginService, [FromServices] ContactService contactService, [FromServices] CostService costService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            var userId = this.HttpContext.Session.GetInt32("userId");
            Cost? cost = id == null ? new Cost() { UserId = userId } : await costService.GetCost(id ?? -1);
            if (id != null && (cost == null || cost.UserId != userId))
            {
                ViewData["Error"] = "Tento výdaj neexistuje";
                return await this.RenderForm((int)userId, cost != null ? (int?)cost.Id : null, costForm, contactService);
            }
            if (!ModelState.IsValid)
            {
                ViewData["Error"] = "Opravte jednotlivé chyby";
                return await this.RenderForm((int)userId, cost != null ? (int?)cost.Id : null, costForm, contactService);
            }
            Mapper.CopyProperties(costForm, cost);
            cost.UserId = userId;
            bool result = id == null ? await costService.InsertCost(cost) : await costService.UpdateCost(cost);
            if (result)
            {
                if (id == null)
                {
                    this.HttpContext.Session.SetInt32("alert_costAdded", 1);
                    return Redirect($"/costs/edit/{cost.Id}");
                }
                ViewData["Success"] = "Výdaj uložen";
            }
            else
            {
                ViewData["Error"] = "Výdaj se nepodařilo uložit";
            }

            return await this.RenderForm((int)userId, cost != null ? (int?)cost.Id : null, costForm, contactService);
        }

        public async Task<IActionResult> RenderForm(int userId, int? costId, CostForm costForm, ContactService contactService)
        {
            ViewBag.CostId = costId;
            ViewBag.Types = new List<SelectListItem>()
            {
                new SelectListItem("Faktura", "0"),
                new SelectListItem("Účtenka", "2"),
                new SelectListItem("Jiný", "3"),
            };
            ViewBag.Suppliers = new List<SelectListItem>();
            foreach (var supplier in await contactService.GetSuppliers(userId))
            {
                ViewBag.Suppliers.Add(
                    new SelectListItem(supplier.ToString(), supplier.Id.ToString())
                );
            }
            return View(costForm);
        }

    }
}
