﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Projekt2.Models;
using System.Threading.Tasks;

namespace Projekt2.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Index([FromServices] LoginService loginService)
        {
            if (loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/");
            }
            if (this.HttpContext.Session.GetInt32("alert_loggedOut") != null)
            {
                this.HttpContext.Session.Remove("alert_loggedOut");
                ViewData["Success"] = "Byli jste úspěšně odhlášeni";
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(LoginForm loginForm, [FromServices] LoginService loginService)
        {
            if (loginForm.Username == null)
            {
                ModelState.AddModelError("Username", "Vyplňte uživatelské jméno");
            }
            if (loginForm.Password == null)
            {
                ModelState.AddModelError("Username", "Vyplňte heslo");
            }
            if (loginForm.Username == null || loginForm.Password == null)
            {
                return View();
            }

            bool loginResult = await loginService.Auth(loginForm.Username, loginForm.Password, this.HttpContext.Session);

            if (loginResult)
            {
                return Redirect("/");
            }
            ModelState.AddModelError("Username", "Špatné jméno nebo heslo");
            return View();
        }

        [HttpPost]
        public IActionResult Logout()
        {
            this.HttpContext.Session.Clear();
            this.HttpContext.Session.SetInt32("alert_loggedOut", 1);
            return Redirect("/login");
        }
    }
}
