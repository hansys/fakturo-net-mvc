﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Projekt2.Models;
using Projekt2.Models.Database;
using Projekt2.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Projekt2.Controllers
{
    public class InvoicesController : Controller
    {
        [HttpGet]
        [ActionName("Index")]
        public async Task<IActionResult> IndexGet([FromServices] LoginService loginService, [FromServices] InvoiceService invoiceService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            var userId = this.HttpContext.Session.GetInt32("userId");
            var invoices = await invoiceService.GetInvoices(userId ?? -1);
            ViewBag.Invoices = invoices;
            return View();
        }

        [HttpPost]
        [ActionName("Index")]
        public async Task<IActionResult> IndexPost([FromServices] LoginService loginService, [FromServices] InvoiceService invoiceService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            var userId = this.HttpContext.Session.GetInt32("userId");

            if (this.HttpContext.Request.Form.ContainsKey("deleteInvoice"))
            {
                int id = Convert.ToInt32(this.HttpContext.Request.Form["deleteInvoice"]);
                var invoice = await invoiceService.GetInvoice(id);
                if (invoice != null && invoice.UserId == userId)
                {
                    bool result = await invoiceService.DeleteInvoice(invoice);
                    if (result)
                    {
                        ViewData["Success"] = "Faktura byla smazána";
                    }
                    else
                    {
                        ViewData["Error"] = "Fakturu se nepodařilo smazat";
                    }
                }
            }

            var invoices = await invoiceService.GetInvoices(userId ?? -1);
            ViewBag.Invoices = invoices;
            return View();
        }

        [HttpGet]
        [ActionName("Edit")]
        public async Task<IActionResult> EditGet(int? id, [FromServices] LoginService loginService, [FromServices] ContactService contactService, [FromServices] InvoiceService invoiceService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            InvoiceForm invoiceForm = new InvoiceForm();
            var userId = this.HttpContext.Session.GetInt32("userId");
            var invoice = await invoiceService.GetInvoice(id ?? -1);
            if (id != null && (invoice == null || invoice.UserId != userId))
            {
                ViewData["Error"] = "Tato faktura neexistuje";
                return await this.RenderForm((int)userId, id, invoiceForm, contactService);
            }
            if (invoice != null && invoice.UserId == userId)
            {
                foreach (var invoiceItem in await invoiceService.GetInvoiceItems((int)invoice.Id))
                {
                    var invoiceItemForm = new InvoiceItemForm();
                    Mapper.CopyProperties(invoiceItem, invoiceItemForm);
                    invoiceForm.Items.Add(invoiceItemForm);
                }
                Mapper.CopyProperties(invoice, invoiceForm);
            }
            if (this.HttpContext.Session.GetInt32("alert_invoiceAdded") != null)
            {
                this.HttpContext.Session.Remove("alert_invoiceAdded");
                ViewData["Success"] = "Faktura byla uložena";
            }
            return await this.RenderForm((int)userId, invoice != null ? (int?)invoice.Id : null, invoiceForm, contactService);
        }

        [HttpPost]
        [ActionName("Edit")]
        public async Task<IActionResult> EditPost(int? id, InvoiceForm invoiceForm, [FromServices] LoginService loginService, [FromServices] ContactService contactService, [FromServices] InvoiceService invoiceService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            var userId = this.HttpContext.Session.GetInt32("userId");

            if (this.HttpContext.Request.Form.ContainsKey("addInvoiceItem"))
            {
                invoiceForm.Items.Add(new InvoiceItemForm());
            }

            if (this.HttpContext.Request.Form.ContainsKey("deleteInvoiceItem"))
            {
                int index = Convert.ToInt32(this.HttpContext.Request.Form["deleteInvoiceItem"]);
                if (index >= 0 && index < invoiceForm.Items.Count)
                {
                    invoiceForm.Items[index].Deleted = true;
                    invoiceForm.Items[index].Name = "Deleted";
                    invoiceForm.Items[index].Quantity = 0;
                    invoiceForm.Items[index].Unit = "Deleted";
                    invoiceForm.Items[index].Price = 0;
                    invoiceForm.Items[index].TaxPercentageRate = 0;
                    ModelState.Clear();
                }
            }

            if (this.HttpContext.Request.Form.ContainsKey("save"))
            {
                Invoice? invoice = id == null ? new Invoice() { UserId = userId } : await invoiceService.GetInvoice(id ?? -1);
                if (id != null && (invoice == null || invoice.UserId != userId))
                {
                    ViewData["Error"] = "Tato faktura neexistuje";
                    return await this.RenderForm((int)userId, id, invoiceForm, contactService);
                }
                if (invoiceForm.CreatedTime > invoiceForm.PayTillTime)
                {
                    ModelState.AddModelError("PayTillTime", "Datum splatnosti musí být větší nebo rovno datu vystavení");
                }
                if (!ModelState.IsValid)
                {
                    ViewData["Error"] = "Opravte jednotlivé chyby";
                    return await this.RenderForm((int)userId, id, invoiceForm, contactService);
                }

                invoiceForm.TotalPrice = invoiceForm.Items.Where(x => !x.Deleted).Sum(x => x.TotalPrice);
                invoiceForm.TotalPriceWithoutTax = invoiceForm.Items.Where(x => !x.Deleted).Sum(x => x.TotalPriceWithoutTax);
                invoiceForm.IsPaid = invoiceForm.TotalPrice == invoiceForm.TotalPaidPrice;
                Mapper.CopyProperties(invoiceForm, invoice);
                ModelState.Clear();

                var invoiceResult = invoice.Id == null ? await invoiceService.InsertInvoice(invoice) : await invoiceService.UpdateInvoice(invoice);
                if (!invoiceResult)
                {
                    ViewData["Error"] = "Fakturu se nepodařilo aktualizovat";
                    return await this.RenderForm((int)userId, id, invoiceForm, contactService);
                }

                List<InvoiceItem> invoiceItems = new List<InvoiceItem>();
                foreach (var invoiceItemForm in invoiceForm.Items)
                {
                    InvoiceItem invoiceItem = new InvoiceItem()
                    {
                        InvoiceId = invoice.Id
                    };
                    Mapper.CopyProperties(invoiceItemForm, invoiceItem);
                    invoiceItems.Add(invoiceItem);
                }
                var invoiceItemsResult = invoiceService.SaveInvoiceItems(invoiceItems);


                if (id == null)
                {
                    this.HttpContext.Session.SetInt32("alert_invoiceAdded", 1);
                    return Redirect($"/invoices/edit/{invoice.Id}");
                }
                ViewData["Success"] = "Faktura byla uložena";
            }
            return await this.RenderForm((int)userId, id, invoiceForm, contactService);
        }

        public async Task<IActionResult> RenderForm(int userId, int? invoiceId, InvoiceForm invoiceForm, ContactService contactService)
        {
            ViewBag.InvoiceId = invoiceId;
            ViewBag.PaymentTypes = new List<SelectListItem>()
            {
                new SelectListItem("Převodem", "0"),
                new SelectListItem("Hotově", "2"),
                new SelectListItem("Platební kartou", "3"),
            };
            ViewBag.Subscribers = new List<SelectListItem>();
            foreach (var subcriber in await contactService.GetSubscribers(userId))
            {
                ViewBag.Subscribers.Add(
                    new SelectListItem(subcriber.ToString(), subcriber.Id.ToString())
                );
            }
            return View(invoiceForm);
        }

    }
}
