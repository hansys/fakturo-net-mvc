﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Projekt2.Models;
using Projekt2.Models.Database;
using Projekt2.Models.DataModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace Projekt2.Controllers
{
    public class UsersController : Controller
    {
        [HttpGet]
        [ActionName("Index")]
        public async Task<IActionResult> IndexGet([FromServices] LoginService loginService, [FromServices] UserService userService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            if (this.HttpContext.Session.GetInt32("permission") != 1)
            {
                return Redirect("/");
            }
            var userId = this.HttpContext.Session.GetInt32("userId");
            var users = await userService.GetUsers();
            ViewBag.Users = users;
            ViewBag.UserId = userId;
            return View();
        }

        [HttpPost]
        [ActionName("Index")]
        public async Task<IActionResult> IndexPost([FromServices] LoginService loginService, [FromServices] UserService userService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            if (this.HttpContext.Session.GetInt32("permission") != 1)
            {
                return Redirect("/");
            }
            if (this.HttpContext.Request.Form.ContainsKey("deleteUser"))
            {
                int id = Convert.ToInt32(this.HttpContext.Request.Form["deleteUser"]);
                User? user = await userService.GetUser(id);
                if (user != null)
                {
                    bool result = await userService.DeleteUser(user);
                    if (result)
                    {
                        ViewData["Success"] = "Uživatel byl smazán";
                    }
                    else
                    {
                        ViewData["Error"] = "Uživatele se nepodařilo smazat";
                    }
                }
            }
            var userId = this.HttpContext.Session.GetInt32("userId");
            var users = await userService.GetUsers();
            ViewBag.Users = users;
            ViewBag.UserId = userId;
            return View();
        }


        [HttpGet]
        [ActionName("Edit")]
        public async Task<IActionResult> EditGet(int? id, [FromServices] LoginService loginService, [FromServices] UserService userService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            var userId = this.HttpContext.Session.GetInt32("userId");
            var permission = this.HttpContext.Session.GetInt32("permission");

            UserForm userForm = new UserForm();
            User? user = id == null ? new User() : await userService.GetUser(id ?? -1);
            if (user != null && user.Id != user.Id && permission != 1)
            {
                ViewData["Error"] = "Pro editaci tohoto uživatele nemáte oprávnění";
                return await this.RenderForm(user != null ? user.Id : null, userForm);
            }
            if (this.HttpContext.Session.GetInt32("alert_userAdded") != null)
            {
                this.HttpContext.Session.Remove("alert_userAdded");
                ViewData["Success"] = "Uživatel byl vytvořen";
            }
            if (id != null && user == null)
            {
                ViewData["Error"] = "Tento uživatel neexistuje";
                return await this.RenderForm(user != null ? user.Id : null, userForm);
            }
            Mapper.CopyProperties(user, userForm);
            return await this.RenderForm(user != null ? user.Id : null, userForm);
        }

        [HttpPost]
        [ActionName("Edit")]
        public async Task<IActionResult> EditPost(int? id, UserForm userForm, [FromServices] LoginService loginService, [FromServices] UserService userService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            var userId = this.HttpContext.Session.GetInt32("userId");
            var permission = this.HttpContext.Session.GetInt32("permission");
            var username = this.HttpContext.Session.GetString("username");

            User? user = id == null ? new User() : await userService.GetUser(id ?? -1);
            if (user != null && user.Id != user.Id && permission != 1)
            {
                ViewData["Error"] = "Pro editaci tohoto uživatele nemáte oprávnění";
                return await this.RenderForm(user != null ? user.Id : null, userForm);
            }
            if (id != null && user == null)
            {
                ViewData["Error"] = "Tento uživatel neexistuje";
                return await this.RenderForm(user != null ? user.Id : null, userForm);
            }
            if (!ModelState.IsValid)
            {
                ViewData["Error"] = "Opravte jednotlivé chyby";
                return await this.RenderForm(user != null ? user.Id : null, userForm);
            }

            Mapper.CopyProperties(userForm, user);
            if (user.Id == userId)
            {
                user.Permission = permission ?? 0;
                user.Username = username;
            }
            if (userForm.Password != null && userForm.Password.Length > 0)
            {
                bool passResult = await userService.SetPassword(user, userForm.Password, userForm.PasswordAgain);
                if (!passResult)
                {
                    ViewData["Error"] = "Hesla se neshodují";
                    return await this.RenderForm(user != null ? user.Id : null, userForm);
                }
            }

            bool result = id == null ? await userService.InsertUser(user) : await userService.UpdateUser(user);
            if (result)
            {
                if (id == null)
                {
                    this.HttpContext.Session.SetInt32("alert_userAdded", 1);
                    return Redirect($"/users/edit/{user.Id}");
                }
                ViewData["Success"] = "Uživatel uložen";
            }
            else
            {
                ViewData["Error"] = "Uživatele se nepodařilo uložit";
            }
            return await this.RenderForm(user != null ? user.Id : null, userForm);
        }


        public async Task<IActionResult> RenderForm(long? userId, UserForm userForm)
        {
            ViewBag.UserId = userId;
            ViewBag.Permissions = new List<SelectListItem>()
            {
                new SelectListItem("Uživatel", "0"),
                new SelectListItem("Administrátor", "1")
            };
            return View(userForm);
        }
    }
}
