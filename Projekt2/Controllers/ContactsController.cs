﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Projekt2.Models;
using Projekt2.Models.Database;
using Projekt2.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projekt2.Controllers
{
    public class ContactsController : Controller
    {
        [HttpGet]
        [ActionName("Index")]
        public async Task<IActionResult> IndexGet([FromServices] LoginService loginService, [FromServices] ContactService contactService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }

            var userId = this.HttpContext.Session.GetInt32("userId");
            var contacts = await contactService.GetContacts(userId ?? -1);
            ViewBag.Contacts = contacts;
            ViewBag.UserContactId = this.HttpContext.Session.GetInt32("userContactId");
            return View();
        }

        [HttpPost]
        [ActionName("Index")]
        public async Task<IActionResult> IndexPost([FromServices] LoginService loginService, [FromServices] ContactService contactService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }

            var userId = this.HttpContext.Session.GetInt32("userId");
            if (this.HttpContext.Request.Form.ContainsKey("deleteContact"))
            {
                var contact = await contactService.GetContact(Convert.ToInt32(this.HttpContext.Request.Form["deleteContact"]));
                if (contact != null && contact.UserId == userId)
                {
                    var result = await contactService.DeleteContact(contact);
                    if (result)
                    {
                        ViewData["Success"] = "Kontakt byl úspěšně smazán";
                    }
                    else
                    {
                        ViewData["Error"] = "Kontakt se nepodařilo smazat";
                    }
                }
                else
                {
                    ViewData["Error"] = "Nemáte oprávnění smazat tento kontakt";
                }
            }
            var contacts = await contactService.GetContacts(userId ?? -1);
            ViewBag.Contacts = contacts;
            return View();
        }


        public IActionResult RenderForm(Contact? contact, ContactForm contactForm)
        {
            ViewBag.ContactId = contact != null ? contact.Id : null;
            ViewBag.ContactType = new List<SelectListItem>()
            {
                new SelectListItem("Odběratel", "1"),
                new SelectListItem("Dodavatel", "2"),
                new SelectListItem("Odběratel i dodavatel", "0"),
            };
            ViewBag.CountryCodes = new List<SelectListItem>()
            {
                new SelectListItem("Česká republika", "CZ"),
                new SelectListItem("Slovensko", "SK"),
            };
            ViewBag.IsPersonal = contactForm.IsPersonal.ToString().ToLower();
            return View(contactForm);
        }

        public async Task<IActionResult> Edit(int? id, [FromServices] LoginService loginService, [FromServices] ContactService contactService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            ContactForm contactForm = new ContactForm();
            var userId = this.HttpContext.Session.GetInt32("userId");
            var contact = id != null ? await contactService.GetContact(id ?? -1) : new Contact();
            if (id == null || (contact != null && contact.UserId == userId))
            {
                if (this.HttpContext.Session.GetInt32("alert_contactAdded") != null)
                {
                    this.HttpContext.Session.Remove("alert_contactAdded");
                    ViewData["Success"] = "Kontakt úspěšně přidán";
                }
                Mapper.CopyProperties(contact, contactForm);
            }
            else
            {
                ViewData["Error"] = "Kontakt nenalezen";
            }
            return this.RenderForm(contact, contactForm);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int? id, ContactForm contactForm, [FromServices] LoginService loginService, [FromServices] ContactService contactService)
        {
            if (!loginService.IsLoggedIn(this.HttpContext.Session))
            {
                return Redirect("/login");
            }
            var userId = this.HttpContext.Session.GetInt32("userId");
            Contact? editedContact;
            if (id == null)
            {
                editedContact = new Contact();
                editedContact.UserId = userId;
            }
            else
            {
                editedContact = await contactService.GetContact((int)id);
                if (editedContact == null || editedContact.UserId != userId)
                {
                    ViewData["Error"] = "Kontakt nenalezen";
                    return this.RenderForm(editedContact, contactForm);
                }
            }
            if (contactForm.IsPersonal && (contactForm.FirstName == null || contactForm.FirstName.Length == 0))
            {
                ModelState.AddModelError("FirstName", "Jméno je povinný údaj");
            }
            if (contactForm.IsPersonal && (contactForm.LastName == null || contactForm.LastName.Length == 0))
            {
                ModelState.AddModelError("LastName", "Příjmení je povinný údaj");
            }
            if (!contactForm.IsPersonal && (contactForm.Company == null || contactForm.Company.Length == 0))
            {
                ModelState.AddModelError("Company", "Společnost je povinný údaj");
            }
            if (!ModelState.IsValid)
            {
                ViewData["Error"] = "Opravte jednotlivé chyby";
                return this.RenderForm(editedContact, contactForm);
            }
            Mapper.CopyProperties(contactForm, editedContact);
            bool result;
            if (id == null)
            {
                result = await contactService.InsertContact(editedContact);
                if (result)
                {
                    this.HttpContext.Session.SetInt32("alert_contactAdded", 1);
                    return Redirect($"/contacts/edit/{editedContact.Id}");
                }
            }
            else
            {
                result = await contactService.UpdateContact(editedContact);
            }

            if (result)
            {
                ViewData["Success"] = "Změny úspěšně uloženy";
            }
            else
            {
                ViewData["Error"] = "Kontakt se nepodařilo uložit";
            }
            return this.RenderForm(editedContact, contactForm);
        }
    }
}
