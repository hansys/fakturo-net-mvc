﻿using Projekt2.Models.Database;
using Projekt2.Models.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Projekt2
{
    public class InvoiceService
    {
        public async Task<ObservableCollection<Invoice>> GetInvoices(int userId)
        {
            var db = Singleton.Database;
            var invoices = await db.SelectList<Invoice>(userId, "UserId");
            foreach (var invoice in invoices)
            {
                await invoice.LoadSubscriber(Singleton.Database);
            }
            return invoices;
        }

        public async Task<Invoice?> GetInvoice(int id)
        {
            var db = Singleton.Database;
            return await db.Select<Invoice>(id);
        }

        public async Task<ObservableCollection<InvoiceItem>> GetInvoiceItems(int invoiceId)
        {
            var db = Singleton.Database;
            return await db.SelectList<InvoiceItem>(invoiceId, "InvoiceId");
        }

        public async Task<bool> SaveInvoiceItems(IEnumerable<InvoiceItem> invoiceItems)
        {
            var db = Singleton.Database;
            bool result = true;
            foreach (var invoiceItem in invoiceItems)
            {
                if (invoiceItem.Deleted)
                {
                    if (invoiceItem.Id != null)
                    {
                        result = await db.Delete(invoiceItem);
                    }
                    continue;
                }
                if (invoiceItem.Id == null)
                {
                    result = await db.Insert(invoiceItem);
                }
                else
                {
                    result = await db.Update(invoiceItem);
                }
                if (!result)
                {
                    return false;
                }
            }
            return true;
        }

        public async Task<bool> InsertInvoice(Invoice invoice)
        {
            var db = Singleton.Database;
            var thisYearTimeStamp = Mapper.DateTimeToUnixTimestamp(
                    new DateTime(DateTime.Now.Year, 1, 1)
                ); ;
            var year = DateTime.Now.ToString("yy");
            var count = await db.Count<Invoice>((int)invoice.UserId, thisYearTimeStamp, "CreatedTime") + 1 ?? 1;
            var countString = count.ToString("D4");
            invoice.InvoiceNum = $"F{year}{countString}";
            return await db.Insert(invoice);
        }

        public async Task<bool> UpdateInvoice(Invoice invoice)
        {
            var db = Singleton.Database;
            return await db.Update(invoice);
        }

        public async Task<bool> DeleteInvoice(Invoice invoice)
        {
            var db = Singleton.Database;
            var invoiceItems = await this.GetInvoiceItems((int)invoice.Id);
            foreach (var invoiceItem in invoiceItems)
            {
                await db.Delete(invoiceItem);
            }
            return await db.Delete(invoice);
        }
    }
}
