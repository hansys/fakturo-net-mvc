﻿using Microsoft.AspNetCore.Http;
using Projekt2.Models.DataModel;
using System;
using System.Threading.Tasks;

namespace Projekt2
{
    public class LoginService
    {
        public object HttpContext { get; private set; }

        public async Task<bool> Auth(string username, string password, ISession? session = null)
        {
            
            var db = Singleton.Database;
            User? user = await db.Select<User>(username, "Username");
            if (user != null)
            {
                var result = BCrypt.Net.BCrypt.Verify(password, user.PasswordHash);
                if (result)
                {
                    if (session != null)
                    {
                        session.SetInt32("loggedIn", 1);
                        session.SetInt32("userId", Convert.ToInt32(user.Id));
                        session.SetString("username", Convert.ToString(user.Username));
                        session.SetInt32("userContactId", Convert.ToInt32(user.ContactId));
                        session.SetInt32("permission", user.Permission);
                    }
                    return true;
                }
            }
            return false;
        }

        public bool IsLoggedIn(ISession session)
        {
            int? loggedIn = session.GetInt32("loggedIn");
            return loggedIn != null && loggedIn == 1;
        }
    }
}
