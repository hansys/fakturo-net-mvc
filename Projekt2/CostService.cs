﻿using Projekt2.Models.Database;
using Projekt2.Models.DataModel;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Projekt2
{
    public class CostService
    {
        public async Task<ObservableCollection<Cost>> GetCosts(int userId)
        {
            var db = Singleton.Database;
            var costs = await db.SelectList<Cost>(userId, "UserId");
            foreach (var cost in costs)
            {
                await cost.LoadSupplier(Singleton.Database);
            }
            return costs;
        }

        public async Task<Cost?> GetCost(int id)
        {
            var db = Singleton.Database;
            return await db.Select<Cost>(id);
        }

        public async Task<bool> InsertCost(Cost cost)
        {
            var db = Singleton.Database;
            var thisYearTimeStamp = Mapper.DateTimeToUnixTimestamp(
                new DateTime(DateTime.Now.Year, 1, 1)
            );
            var year = DateTime.Now.ToString("yy");
            var count = await db.Count<Cost>((int)cost.UserId, thisYearTimeStamp, "CreatedTime") + 1 ?? 1;
            var countString = count.ToString("D4");
            cost.CostNum = $"N{year}{countString}";
            return await db.Insert(cost);
        }

        public async Task<bool> UpdateCost(Cost cost)
        {
            var db = Singleton.Database;
            return await db.Update(cost);
        }

        public async Task<bool> DeleteCost(Cost cost)
        {
            var db = Singleton.Database;
            return await db.Delete(cost);
        }

    }
}
