﻿using Projekt2.Models.DataModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Projekt2
{
    public class ContactService
    {
        public async Task<ObservableCollection<Contact>> GetContacts(int userId)
        {
            var db = Singleton.Database;
            var contacts = await db.SelectList<Contact>(userId, "UserId");
            return contacts;
        }

        public async Task<Contact?> GetContact(int contactId)
        {
            var db = Singleton.Database;
            return await db.Select<Contact>(contactId);
        }

        public async Task<bool> InsertContact(Contact contact)
        {
            var db = Singleton.Database;
            return await db.Insert(contact);
        }

        public async Task<bool> UpdateContact(Contact contact)
        {
            var db = Singleton.Database;
            return await db.Update(contact);
        }

        public async Task<bool> DeleteContact(Contact contact)
        {
            var db = Singleton.Database;
            return await db.Delete(contact);
        }

        public async Task<ObservableCollection<Contact>> GetSubscribers(int userId)
        {
            var db = Singleton.Database;
            var contacts = await db.SelectList<Contact>(userId, "UserId");
            for (int i = contacts.Count -1; i >= 0; i--)
            {
                var contact = contacts[i];
                if (!contact.IsSubscriber)
                {
                    contacts.Remove(contact);
                }
            }
            return contacts;
        }

        public async Task<ObservableCollection<Contact>> GetSuppliers(int userId)
        {
            var db = Singleton.Database;
            var contacts = await db.SelectList<Contact>(userId, "UserId");
            for (int i = contacts.Count - 1; i >= 0; i--)
            {
                var contact = contacts[i];
                if (!contact.IsSupplier)
                {
                    contacts.Remove(contact);
                }
            }
            return contacts;
        }

    }
}
