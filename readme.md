# Fakturo
#### Simple web information system for freelancers written in .NET Core MVC.

## UI Screens
<img src="screen1.png" alt="Screen 1">

<img  src="screen2.png" alt="Screen 2">

<img src="screen3.png" alt="Screen 3">
